<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public $table = "table_review";
    protected $fillable = ['id','product_id','experience','arrive_on_time','quality','response_time','comment'];
    protected $hidden = ['created_at','updated_at'];

    public function product(){
        return $this->belongsTo(Product::class,'product_id')->select(['id','company_id','title','subtitle','condition','feature_photo',
        'price','key_words','model','brand','description']);
    }


}

?>