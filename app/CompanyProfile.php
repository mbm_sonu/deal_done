<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class CompanyProfile extends Model{

    public $table="table_company_profile";
    protected $fillable=['id','user_id','city_id','street_name','logo','tag_line','description','phone_no','company_name'];
    protected $hidden=['updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id')->select(['id','name','number','profile_pic']);
    }
    public function city()
    {
        return $this->belongsTo(City::class,'city_id')->with('state');
    }
    public function getLogoAttribute($value)
    {
        return imgRoute::url().$value;
    }

    
}





?>