<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Country extends Model{

    public $table="table_country";
    protected $fillable=['id','country_name'];
    protected $hidden=['created_at','updated_at'];

    public function state()
    {
        return $this->hasMany(State::class,'country_id');
    }

}


?>