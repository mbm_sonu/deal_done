<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = "table_category";
    protected $fillable = ['id','category_name','logo'];
    protected $hidden = ['created_at','updated_at'];

   
    public function subCategory()
    {
        return $this->hasMany(SubCategory::class,'category_id');
    }
    public function products()
    {
        return $this->hasMany(Product::class,'category_id');
    }
        public function getLogoAttribute($value)
    {
        return imgRoute::url().$value;
    }

}

?>