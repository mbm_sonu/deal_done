<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class City extends Model{

    public $table="table_city";
    protected $fillable=['id','city_name','state_id'];
    protected $hidden=['created_at','updated_at'];

    
    public function  state(){
        return $this->belongsTo(State::class,'state_id')->select(['id','state_name','country_id']);
    }

    public function address()
    {
        return $this->hasMany(Address::class,'city_id');
    }


    // public function product()
    // {
    //     $users = City::table('table_city')
    //         ->join('table_address', 'table_city.id', '=', 'table_address.city_id')
    //         ->join('table_company_profile', 'table_address.id', '=', 'table_company_profile.address_id')
    //         ->join('table_product', 'table_company_profile.id', '=', 'table_product.company_id')
    //         ->select('table_product.title')
    //         ->get();
    //         return $users;
    // }

}




?>