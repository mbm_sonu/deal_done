<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
//use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
//use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
class User extends Model// implements Authenticatable//AuthenticatableContract, AuthorizableContract
{
    public $table = "table_user";
   // use AuthenticableTrait;
    //use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'number','password','auth_token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','created_at','updated_at'
    ];

    public function products()
    {
        return $this->hasMany(Product::class,'user_id');
    }
    public function address()
    {
        return $this->hasMany(Address::class,'user_id')->with('city');
    }
    public function company()
    {
        return $this->hasOne(CompanyProfile::class,'user_id');
    }
}
