<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Photos_table extends Model
{
    public $table = "table_photos_table";
    protected $fillable = ['id','link','product_id'];
    protected $hidden = ['created_at','updated_at'];

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id')->select(['id','title','subtitle','condition','feature_photo','price','key_words','model','brand','description']);
    }
}

?>