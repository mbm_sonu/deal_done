<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model{

    public $table="table_vendor";
    protected $fillable=['id','vendor_name','mobile_no','profile_pic'];
    protected $hidden=['created_at','updated_at'];

    public function company_profile()
    {
        return $this->hasMany(Company_profile::class,'user_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class,'user_id');
    }

}

?>