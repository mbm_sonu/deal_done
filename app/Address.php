<?php
namespace App;
use illuminate\Database\Eloquent\Model;

class Address extends Model{

    public $table="table_address";
    protected $fillable=['id','street_name','city_id','user_id'];
    protected $hidden=['created_at','updated_at'];

    
    public function  city(){
        return $this->belongsTo(City::class,'city_id')->with('state');
    }
    // public function  state(){
    //     return $this->belongsTo(State::class,'state_id')->select(['id','state_name']);
    // }

    public function user(){
        return $this->belongsTo(User::class,'user_id')->select(['id','name','state_id']);
    }

    // public function company()
    // {
    //     return $this->hasMany(Company_profile::class,'address_id');
    // }
}




?>