<?php
namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\Http\Request;
use App\Discount;

class DiscountController extends Controller
{
    public function getAll()
    {
        return response()->json(Discount::with('product')->get());
    }

    public function getById($id)
    {
        return response()->json(Discount::with('product')->findorfail($id),200);
    }

    public function getByProduct($id)
    {
        return response()->json(Discount::with('product')->findorfail($id),200);
    }

    public function create(Request $request)
    {
        $this->validate($request,['product_id'=>'required','discount'=>'required']);
        $discount = Discount::where('product_id', $request->input('product_id'))->first();
        if($discount)
        {
            $this->update($discount->id,$request);
        }
        else
        {
            $discount = Discount::create($request->all());
        }
        return $this->getById($discount->id);
    }

    public function update($id, Request $request)
    {
        $this->validate($request,['product_id'=>'required','discount'=>'required']);
        //$head = $request->all();
        $item = Discount::findOrFail($id);
        $item->update($request->all());
        return $this->getById($item->id);
        // return response()->json($item, 200);
    }

    public function delete($id)
    {
        Discount::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

?>