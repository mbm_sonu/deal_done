<?php
namespace App\http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\http\Request;
use Illuminate\Support\Facades\File;
use App\CompanyProfile;
use App\imgRoute as imgRoute;
use Intervention\Image\ImageManagerStatic as Image;

class Company_profile_Controller extends Controller{

    public function getAll()
    {
        return response()->json(CompanyProfile::with('city')->with('user')->get());
    }

    public function getAddresses($id)
    {
        return response()->json(CompanyProfile::with('city')->findorfail($id),200);
    }

    
    public function getByUser($id)
    {
        return response()->json($this->getByUserId($id),200);
    }

    // public function getProducts($id)
    // {
    //     return response()->json(CompanyProfile::with('product')->findorfail($id),200);
    // }

    public function getById($id)
    {
        return response()->json(CompanyProfile::with('user')->with('city')->findorfail($id),200);
    }

    public function getByUserId($id)
    {
        return CompanyProfile::where('user_id', $id)->with('city')->first(); 
    }
    public function create(Request $request)
    {
        $this->validate($request,['user_id'=>'required','city_id'=>'required','company_name'=>'required','logo'=>'required']);
        $data = $request->toArray();
        $logo = $data['logo'];
        if(filter_var($logo, FILTER_VALIDATE_URL))
        {
            $name = basename($logo);
            $data['logo'] = $name; 
        }
        else
        {
            $name = imgRoute::saveImage($request->logo);
            if($name != "")
            {
                $data['logo'] = $name;
            }
            else
            {
                return response("Image not uploaded",403);
            }
        }

        $profile = $this->getByUserId($request->input('user_id'));        
        if($profile)
        {
            $item = CompanyProfile::findOrFail($profile->id);
            $item->update($data);
            return $this->getById($item->id);
        }
        else{
            $item = CompanyProfile::create($data);
            return $this->getById($item->id);
        } 

        
    


        // $imagePath =  imgRoute::url().'placeholder.jpg';

        // if($request->logo)
        // {
        //     $data = explode('/', explode(';' , $request->logo)[0]);
        //     $extension = $data[1];
        //     $type = explode(':', $data[0])[1];
        //     if($type != 'image'){
                
        //         return response()->json(['logo' => 'File Uploaded is not an image!']);
        //     } else {
                
        //         Image::configure(array('driver' => 'gd'));
        //         $random_number = mt_rand( 10000000, 99999999);
        //         $name = $random_number . '_' .time() . '.' . $extension;
        //         $image_Path = 'images/' . $name;
        //         $imagePath = $name;
        //         $image = Image::make($request->logo)->save($image_Path);
        //         $request->logo = $image_Path;
                
        //         $data = $request->toArray();
        //         $data['logo'] = imgRoute::url().$imagePath;
        //         $item = CompanyProfile::create($data);
        //         return $this->getById($item->id);
        //     }
        // }
        
    }

    public function update($id,Request $request)
    {
        $this->validate($request,['user_id'=>'required','city_id'=>'required','company_name'=>'required','logo'=>'required']);        
        //$imagePath = imgRoute::url().'placeholder.jpg';

        $item = CompanyProfile::findOrFail($id);
        $item->update($request->all());
        return $this->getById($item->id);
        // if($request->logo)
        // {
        //     $data = explode('/', explode(';' , $request->logo)[0]);
        //     $extension = $data[1];
        //     $type = explode(':', $data[0])[1];
        //     if($type != 'image'){
                
        //         return response()->json(['logo' => 'File Uploaded is not an image!']);
        //     } else {

        //         $a=explode("/",$request->image_path)[5];
        //         $b=explode("/",$request->image_path)[6];

        //         $image_path_del=$a."/".$b;

        //         if(File::exists($image_path_del)){
        //             File::delete($image_path_del);
        //         }

        //         Image::configure(array('driver' => 'gd'));
        //         $random_number = mt_rand( 10000000, 99999999);
        //         $name = $random_number . '_' .time() . '.' . $extension;
        //         $image_Path = 'images/' . $name;
        //         $imagePath = $name;
        //         $image = Image::make($request->logo)->save($image_Path);
        //         $request->logo = $image_Path;
                
        //         $data = $request->toArray();
        //         $data['logo'] = imgRoute::url().$imagePath;

        //         $item = CompanyProfile::findOrFail($id);
        //         $item->update($data);
        //         return $this->getById($item->id);
        //     }
    //}
}

    public function delete($id)
    {
     
        $item =Companyrofile::findOrFail($id);
        $a=explode("/",$item->logo)[5];
        $b=explode("/",$item->logo)[6];

        $image_path_del=$a."/".$b;

        if(File::exists($image_path_del)){
            File::delete($image_path_del);
        }
        CompanyProfile::findorfail($id)->delete();
        return response()->json('Deleted Successfully',200);
    }
}









?>