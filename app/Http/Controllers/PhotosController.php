<?php
namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Photos_table;
use Intervention\Image\ImageManagerStatic as Image;
use App\imgRoute as imgRoute;

class PhotosController extends Controller
{
    public function getAll()
    {
        return response()->json(Photos_table::with('product')->get());
    }

    public function getById($id)
    {
        return response()->json(Photos_table::with('product')->findorfail($id),200);
    }

    public function getByProduct($id)
    {
        return response()->json(Photos_table::with('product')->findorfail($id),200);
    }
    
    public function create(Request $request)
    {
        $this->validate($request,['link'=>'required','product_id'=>'required']);
        $imagePath = imgRoute::url().'placeholder.jpg';

        if($request->link)
        {
            $data = explode('/', explode(';' , $request->link)[0]);
            $extension = $data[1];
            $type = explode(':', $data[0])[1];
            if($type != 'image'){
                
                return response()->json(['link' => 'File Uploaded is not an image!']);
            } else {
                Image::configure(array('driver' => 'gd'));
                $random_number = mt_rand( 10000000, 99999999);
                $name = $random_number . '_' .time() . '.' . $extension;
                $image_Path = 'images/' . $name;
                $imagePath = $name;
                $image = Image::make($request->link)->save($image_Path);
                $request->link = $image_Path;
                
                $data = $request->toArray();
                $data['link'] = imgRoute::url().$imagePath;
                $item = Photos_table::create($data);
                return $this->getById($item->id);
            }
        }
    }

    public function update($id, Request $request)
    {
        $this->validate($request,['link'=>'required','product_id'=>'required']);
        //$head = $request->all();
        $imagePath = imgRoute::url().'placeholder.jpg';

        if($request->link)
        {
            $data = explode('/', explode(';' , $request->link)[0]);
            $extension = $data[1];
            $type = explode(':', $data[0])[1];
            if($type != 'image'){
                
                return response()->json(['link' => 'File Uploaded is not an image!']);
            } else {

                $a=explode("/",$request->image_path)[5];
                $b=explode("/",$request->image_path)[6];

                $image_path_del=$a."/".$b;

                if(File::exists($image_path_del)){
                    File::delete($image_path_del);
                }

                Image::configure(array('driver' => 'gd'));
                $random_number = mt_rand( 10000000, 99999999);
                $name = $random_number . '_' .time() . '.' . $extension;
                $image_Path = 'images/' . $name;
                $imagePath = $name;
                $image = Image::make($request->link)->save($image_Path);
                $request->link = $image_Path;
                
                $data = $request->toArray();
                $data['link'] = imgRoute::url().$imagePath;
                
                         
                $item = Photos_table::findOrFail($id);
                $item->update($data);
                return $this->getById($item->id);
            }
    }
}

    public function delete($id)
    {
        $item = Photos_table::findOrFail($id);
        $a=explode("/",$item->link)[5];
        $b=explode("/",$item->link)[6];

        $image_path_del=$a."/".$b;

        if(File::exists($image_path_del)){
            File::delete($image_path_del);
        }
        Photos_table::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

?>