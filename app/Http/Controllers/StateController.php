<?php
namespace App\http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\http\Request;
use App\State;

class StateController extends Controller{

    public function getAll()
    {
        return response()->json(State::all());
    }

    public function getCities($id)
    {
        return response()->json(State::with('city')->findorfail($id),200);
    }

    public function getCountries($id)
    {
        return response()->json(State::with('country')->findorfail($id),200);
    }

    public function getById($id)
    {
        return response()->json(State::findorfail($id),201);
    }

    public function create(Request $request)
    {
        $this->validate($request,['state_name'=>'required','country_id'=>'required']);
        $item=State::create($request->all());
        return $this->getById($item->id);
    }

    public function update($id,Request $request)
    {
        $this->validate($request,['state_name'=>'required','country_id'=>'required']);
        $item=State::findorfail($id);
        $item->update($request->all());
        return $this->getById($id);
    }

    public function delete($id)
    {
        State::findorfail($id)->delete();
        return response()->json('Deleted Successfully',200);
    }
}









?>