<?php
namespace App\http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\http\Request;
use App\Address;
use App\User;


class AddressController extends Controller{

    public function getAll()
    {
        return response()->json(Address::with('city')->get());
    }

    public function getCompanies($id)
    {
        return response()->json(Address::with('company')->findorfail($id),200);
    }

    public function getCities($id)
    {
        return response()->json(Address::with('city')->findorfail($id),200);
    }

    public function getById($id)
    {
        return response()->json(Address::with('city')->findorfail($id),201);
    }

    public function getByUser($id)
    {
        $address = Address::where('user_id', $id)->first();
        return $this->getById($address->id);
    }

    public function create(Request $request)
    {
        $this->validate($request,['street_name'=>'required','city_id'=>'required','user_id'=>'required']);
        $item=Address::create($request->all());
        return $this->getById($item->id);
    }

    public function update($id,Request $request)
    {
        $this->validate($request,['street_name'=>'required']);
        $item=Address::findorfail($id);
        $item->update($request->all());
        return $this->getById($id);
    }

    public function delete($id)
    {
        Address::findorfail($id)->delete();
        return response()->json('Deleted Successfully',200);
    }
}









?>