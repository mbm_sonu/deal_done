<?php
namespace App\http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\http\Request;
use App\City;

class CityController extends Controller{

    public function getAll()
    {
        return response()->json(City::with('state')->get());
    }

    public function getAddresses($id)
    {
        return response()->json(City::with('address')->findorfail($id),200);
    }

    
    // public function getProducts($id)
    // {
    //     return response()->json(City::with('product')->findorfail($id),200);
    // }
    

    public function getStates($id)
    {
        return response()->json(City::with('state')->findorfail($id),200);
    }

    public function getById($id)
    {
        return response()->json(City::with('state')->findorfail($id),201);
    }

    public function create(Request $request)
    {
        $this->validate($request,['city_name'=>'required','state_id'=>'required']);
        $item=City::create($request->all());
        return $this->getById($item->id);
    }

    public function update($id,Request $request)
    {
        $this->validate($request,['city_name'=>'required','state_id'=>'required']);
        $item=City::findorfail($id);
        $item->update($request->all());
        return $this->getById($id);
    }

    public function delete($id)
    {
        City::findorfail($id)->delete();
        return response()->json('Deleted Successfully',200);
    }
}









?>