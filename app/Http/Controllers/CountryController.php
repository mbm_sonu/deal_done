<?php
namespace App\http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\http\Request;
use App\Country;

class CountryController extends Controller{

    public function getAll()
    {
        return response()->json(Country::all());
    }

    public function getStates($id)
    {
        return response()->json(Country::with('state')->findorfail($id),200);
    }

    public function getById($id)
    {
        return response()->json(Country::findorfail($id),201);
    }

    public function create(Request $request)
    {
        $this->validate($request,['country_name'=>'required']);
        $item=Country::create($request->all());
        return $this->getById($item->id);
    }

    public function update($id,Request $request)
    {
        $this->validate($request,['country_name'=>'required']);
        $item=Country::findorfail($id);
        $item->update($request->all());
        return $this->getById($id);
    }

    public function delete($id)
    {
        Country::findorfail($id)->delete();
        return response()->json('Deleted Successfully',200);
    }
}









?>