<?php
namespace App\http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\http\Request;
use Illuminate\Support\Facades\File;
use App\User;

use Intervention\Image\ImageManagerStatic as Image;

use App\imgRoute as imgRoute;

class VendorController extends Controller{


    public function getAll()
    {
        $users = User::with('company')->get();
        $this->filterVendors($users);
        return response()->json($users);
    }

    public function filterVendors($users)
    {
        foreach($users as $key =>$user)
        {
            if(!$user->company)
            {
                unset($users[$key]);
            }
        }
        //return $users;
    }
    public function getCompanies($id)
    {
        return response()->json(Vendor::with('company_profile')->findorfail($id),200);
    }

    public function getById($id)
    {
        return response()->json(User::findorfail($id));
    }

    public function create(Request $request)
    {
        $this->validate($request,['vendor_name'=>'required','mobile_no'=>'required']);
        $imagePath =  imgRoute::url().'placeholder.jpg';

        if($request->profile_pic)
        {
            $data = explode('/', explode(';' , $request->profile_pic)[0]);
            $extension = $data[1];
            $type = explode(':', $data[0])[1];
            if($type != 'image'){
                
                return response()->json(['profile_pic' => 'File Uploaded is not an image!']);
            } else {
                Image::configure(array('driver' => 'gd'));
                $random_number = mt_rand( 10000000, 99999999);
                $name = $random_number . '_' .time() . '.' . $extension;
                $image_Path = 'images/' . $name;
                $imagePath = $name;
                $image = Image::make($request->profile_pic)->save($image_Path);
                $request->profile_pic = $image_Path;
                
                $data = $request->toArray();
                $data['profile_pic'] = imgRoute::url().$imagePath;
                $item = Vendor::create($data);
                return $this->getById($item->id);
            }
        }
    }
    public function getProducts($id)
    {
        return response()->json(Vendor::findorfail($id)->products,200);
    }
    public function update($id,Request $request)
    {
        $this->validate($request,['vendor_name'=>'required','mobile_no'=>'required']);
        $imagePath = imgRoute::url().'placeholder.jpg';

        if($request->profile_pic)
        {
            $data = explode('/', explode(';' , $request->profile_pic)[0]);
            $extension = $data[1];
            $type = explode(':', $data[0])[1];
            if($type != 'image'){
                
                return response()->json(['profile_pic' => 'File Uploaded is not an image!']);
            } else {

                $a=explode("/",$request->image_path)[5];
                $b=explode("/",$request->image_path)[6];

                $image_path_del=$a."/".$b;

                if(File::exists($image_path_del)){
                    File::delete($image_path_del);
                }

                Image::configure(array('driver' => 'gd'));
                $random_number = mt_rand( 10000000, 99999999);
                $name = $random_number . '_' .time() . '.' . $extension;
                $image_Path = 'images/' . $name;
                $imagePath = $name;
                $image = Image::make($request->profile_pic)->save($image_Path);
                $request->profile_pic = $image_Path;
                
                $data = $request->toArray();
                $data['profile_pic'] = imgRoute::url().$imagePath;
                
                $item = Vendor::findOrFail($id);
                $item->update($data);
                return $this->getById($item->id);
            }
    }
    }

    public function delete($id)
    {

        $item = Vendor::findOrFail($id);
        $a=explode("/",$item->profile_pic)[5];
        $b=explode("/",$item->profile_pic)[6];

        $image_path_del=$a."/".$b;

        if(File::exists($image_path_del)){
            File::delete($image_path_del);
        }

        Vendor::findorfail($id)->delete();
        return response()->json('Deleted Successfully',200);
    }

}



?>