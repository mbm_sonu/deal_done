<?php
namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\User;
use App\CompanyProfile;
use App\imgRoute as imgRoute;

use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    protected $companyController;
    public function __construct(Company_profile_Controller $companyController)
    {
        $this->companyController = $companyController;
    }
    public function getAll()
    {
        $products = Product::with('user')->with('category')->with('subCategory')->with('discount')->get();
        $this->calculateDiscount($products); 
        //return response()->json(count($products));
         return response()->json($products);

        //$products = Product::with('user')->with('category')->with('subCategory')->with('discount')->get();

        // foreach($products as $key => $product){
        //     if(count($product->discount)>0)
        //     {
        //         unset($products[$key]['discount']);
        //         $products[$key]->discount = ['percentage'=>10,'id'=>1];
        //         //return $products[$key];
        //     }
        //     else
        //     {
        //         unset($products[$key]['discount']);
        //     }
        // }
        //return response()->json($products);
    }

    private function calculateDiscount($products)
    {
        foreach($products as $key =>$product)
        {
            if($product->discount)
            {
                $start =  $product->discount->start_date;
                $end =  $product->discount->end_date;
                $start = strtotime($start); 
                $end = strtotime($end);
                $current = strtotime( date('Y-m-d'));

                if ($current >= $start && $current <= $end)
                {
                    $discount = $products[$key]['discount'];
                   // unset($products[$key]['discount']);
                    $products[$key]['discount_data'] = ['discounted_price'=>$product->price-($product->price*$discount->discount/100),
                        'end_date'=>$discount->end_date
                ];
                }
                else
                {
                    unset($products[$key]['discount']);
                }
            }

            //return;
            
        }
    }

    // public function getWith()
    // {
    //      $products = Product::with('user')->with('category')->with('subCategory')->with('discount')->get();
    //      //return response()->json($products);

    //     foreach($products as $key =>$product){
    //         if($product->discount)
    //         {
    //         $start =  $product->discount->start_date;
    //         $end =  $product->discount->end_date;
    //         $start = strtotime($start); 
    //         $end = strtotime($end);
    //         $current = strtotime( date('Y-m-d'));

    //             if ($current >= $start && $current <= $end)
    //             {
    //                 $discount = $products[$key]['discount'];
    //                 unset($products[$key]['discount']);
    //                 $products[$key]['discount_data'] = ['discounted_price'=>$product->price-($product->price*$discount->discount/100),
    //                 'end_date'=>$product->$discount->end_date
    //             ];
    //             }
    //             else
    //             {
    //                 unset($products[$key]['discount']);
    //             }
    //         }

    //         //return;
    //      return response()->json($products);
            
    //     }
    // }

    public function getById($id)
    {
        $product = Product::with('photos')->with('user')->with('category')->with('subCategory')->with('discount')->findorfail($id);
            if($product->discount)
            {
            $start =  $product->discount->start_date;
            $end =  $product->discount->end_date;
            $start = strtotime($start); 
            $end = strtotime($end);
            $current = strtotime( date('Y-m-d'));

            if ($current >= $start && $current <= $end)
            {
                //$discount = $product->discount;
                unset($product->discount);
                $product['discount_data'] = ['discounted_price'=>$product->price-($product->price*$product->discount->discount/100),
                'end_date'=>$product->discount->end_date
            ];
            }
            else
            {
                unset($product['discount']);
            }
            }
        // if(count($product->discount)>0)
        // {
        //     unset($product['discount']);
        //     $product->discount = ['percentage'=>10,'id'=>1];
        //     //return $products[$key];
        // }
        // else
        // {
        //     unset($product['discount']);
        // }
        //$company = CompanyProfile::where('user_id', $product->user_id)->first();

        $product['company']=$this->companyController->getByUserId($product->user_id);
    
        return response()->json($product,200);
    }
    public function getByPhotos($id)
    {
        return response()->json(Product::with('photos')->findorfail($id),200);
    }
    public function getByDiscount($id)
    {
        return response()->json(Product::with('discount')->findorfail($id),200);
    }
    public function getByCustom($id)
    {
        return response()->json(Product::with('custom_feature')->findorfail($id),200);
    }

     public function getByUser($id)
    {
        return response()->json(Product::where('user_id', $id)->with('user')->with('category')->with('subCategory')->with('discount')->get(),200);
    }
    
    public function getByCategory($id)
    {
        return response()->json(Product::with('category')->findorfail($id),200);
    }
    public function getBySubCategory($id)
    {
        return response()->json(Product::with('subCategory')->findorfail($id),200);
    }

    public function getProductLike($data)
    {
        $data = urldecode($data);
        $result=Product::where('title','LIKE','%'.$data.'%')
        ->orWhere('keywords','LIKE','%'.$data.'%')
        //->paginate(15)
        ->get();
        return response()->json($result);
    }
    public function create(Request $request)
    {
        $this->validate($request,['user_id'=>'required','title'=>'required','category_id'=>'required'
        ,'condition_name'=>'required','feature_photo'=>'required','price'=>'required','quantity'=>'required',
        'keywords'=>'required','specifications'=>'required',
        'subcategory_id'=>'required'

        ]);

        $imagePath =  imgRoute::url().'placeholder.jpg';
        $data = $request->toArray();
        if($request->feature_photo)
        {
            // $img = $request->feature_photo; // Your data 'data:image/png;base64,AAAFBfj42Pj4';
            // $img = str_replace('data:image/png;base64,', '', $img);
            // $img = str_replace(' ', '+', $img);
            // $img = base64_decode($img);
            // $random_number = mt_rand( 10000000, 99999999);
            // $name = $random_number . '_' .time() . '.png';
            // $image_path = imgRoute::directory() . $name;
            // file_put_contents($image_path, $img);
            
            $name = imgRoute::saveImage($request->feature_photo);
            if($name != "")
            {
                $data['feature_photo'] = $name;
                $data['specifications'] = json_encode($data['specifications']);
                if(strcasecmp($data['condition_name'],'new') == 0)
                {
                    $data['condition'] = 1;
                }
                else if(strcasecmp($data['condition_name'],'old') == 0)
                {
                    $data['condition'] = 0;
                }
                else{
                    return response(['condition' => 'wrong value for condition only new or old is allow'], 400);
                }
                $item = Product::create($data);
                return $this->getById($item->id);
            }
            else{
                 return response()->json(['feature_photo' => 'File Uploaded is not an image!'],400);
            }
            
            // $exploded = explode('/', explode(';' , $request->feature_photo)[0]);
            // if(count($exploded)>1)
            // {
            //     $extension = $exploded[1];
            //     $type = explode(':', $exploded[0])[1];
            //     //echo $extension."  ".$type;
            //     if($type != 'image'){
            //         return response()->json(['feature_photo' => 'File Uploaded is not an image!'],400);
            //     } else {
            //         Image::configure(array('driver' => 'gd'));
            //         $random_number = mt_rand( 10000000, 99999999);
            //         $name = $random_number . '_' .time() . '.' . $extension;
            //         $image_Path = 'images/' . $name;
            //         $imagePath = $name;
                    
            //         $image = Image::make($request->feature_photo)->save($image_Path);
            //         $request->feature_photo = $image_Path;
            //         $data['feature_photo'] = $imagePath;
            //     }
            // }
            // else{
            //     return response()->json(['feature_photo' => 'Bad url formate'],400);
            // }

        }

        // if($request->other_photos)
        // {
        //     $data = explode('/', explode(';' , $request->feature_photo)[0]);
        //     $extension = $data[1];
        //     $type = explode(':', $data[0])[1];
        //     if($type != 'image'){
        //         return response()->json(['feature_photo' => 'File Uploaded is not an image!']);
        //     } else {
        //         Image::configure(array('driver' => 'gd'));
        //         $random_number = mt_rand( 10000000, 99999999);
        //         $name = $random_number . '_' .time() . '.' . $extension;
        //         $image_Path = 'images/' . $name;
        //         $imagePath = $name;
        //         $image = Image::make($request->feature_photo)->save($image_Path);
        //         $request->feature_photo = $image_Path;
        //         $data['feature_photo'] = imgRoute::url().$imagePath;
        //     }
        // }
        
        //$data = $request->toArray();

        
    }

    public function update($id, Request $request)
    {
        $this->validate($request,['user_id'=>'required','title'=>'required','category_id'=>'required'
        ,'condition'=>'required','feature_photo'=>'required','price'=>'required','quantity'=>'required',
        'key_words'=>'required']);
        //$head = $request->all();
        $imagePath =  imgRoute::url().'placeholder.jpg';

        if($request->feature_photo)
        {
            
            $data = explode('/', explode(';' , $request->feature_photo)[0]);
            $extension = $data[1];
            $type = explode(':', $data[0])[1];
            if($type != 'image'){
                
                return response()->json(['feature_photo' => 'File Uploaded is not an image!']);
            } else {

                
                // $a=explode("/",$request->image_path)[5];
                // $b=explode("/",$request->image_path)[6];

                // $image_path_del=$a."/".$b;

                // if(File::exists($image_path_del)){
                //     File::delete($image_path_del);
                // }

                Image::configure(array('driver' => 'gd'));
                $random_number = mt_rand( 10000000, 99999999);
                $name = $random_number . '_' .time() . '.' . $extension;
                $image_Path = 'images/' . $name;
                $imagePath = $name;
                $image = Image::make($request->feature_photo)->save($image_Path);
                $request->feature_photo = $image_Path;
                
                $data = $request->toArray();
                $data['feature_photo'] = imgRoute::url().$imagePath;
                
                $item = Product::findOrFail($id);
                $item->update($data);
                return $this->getById($item->id);
            }
        }
        

        
        // return response()->json($item, 200);
    }

    public function delete($id)
    {
        $item = Product::findOrFail($id);
        $a=explode("/",$item->feature_photo)[5];
        $b=explode("/",$item->feature_photo)[6];

        $image_path_del=$a."/".$b;

        if(File::exists($image_path_del)){
            File::delete($image_path_del);
        }

        Product::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

?>