<?php
namespace App\http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades;
use App\User;
use App\Product;

class UserController extends Controller{
    public function create(Request $request)
    {
        $this->validate($request,[
            'number'=>['required','unique:table_user','min:13'],
            'password'=>['required','min:8'],
            'name'=>'required']);
        
        $item = $request->toArray();
        $item['password'] = Hash::make($item['password']);
        $item['auth_token'] = base64_encode(str_random(40));        
        $item = User::create($item);
        return response()->json($item);
    }
    public function authenticate(Request $request)
    {
        $this->validate($request, [
        'number' => ['required','min:13'],
        'password'=>['required','min:8'],        
            ]);
        $user = User::where('number', $request->input('number'))->first();
        if($user && Hash::check($request->input('password'), $user->password)){
            //$apikey = base64_encode(str_random(40));
            //User::where('number', $request->input('number'))->update(['auth_token' => $apikey]);
            return response()->json($user,200);
        }else{
            return response()->json(['status' => 'number or password is incorrect'],401);
        }
    }
    public function getByToken($token)
    {
        $user = User::where('auth_token', $token)->first();
        return response()->json($user,200);
    }
    public function getAll()
    {
        return response()->json(User::with('company')->get());
    }

    public function getProducts($id)
    {
        return response()->json(User::findorfail($id)->products,200);
    }
    public function getAddress($id)
    {
        return response()->json(User::findorfail($id)->address,200);
    }
    public function getCompany($id)
    {
        return response()->json(User::findorfail($id)->company,200);
    }
    public function getAllVendors()
    {
        
        //$users = app('db')->select("SELECT c.id, user_id, city_id, street_name, logo, tag_line, description, phone_no, company_name, c.created_at, name FROM table_user u, table_company_profile c where c.user_id = u.id");//
        $users = User::whereHas('company')->with('company')->get();
        //$this->filterVendors($users);
        return response()->json($users);
    }

    public function filterVendors($users)
    {
        foreach($users as $key =>$user)
        {
            if(!$user->company)
            {
                unset($users[$key]);
            }
        }
        //return $users;
    }
}









?>