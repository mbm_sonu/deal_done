<?php
namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\Http\Request;
use App\Review;

class ReviewController extends Controller
{
    public function getAll()
    {
        return response()->json(Review::all());
    }

    public function getById($id)
    {
        return response()->json(Review::findorfail($id),200);
    }

    public function getByProduct($id)
    {
        return response()->json(Review::findorfail($id)->product,200);
    }

    public function create(Request $request)
    {
        $this->validate($request,['product_id'=>'required','experience'=>'required','arrive_on_time'=>'required',
        'quality'=>'required','response_time'=>'required','comment'=>'required']);
        $item = Review::create($request->all());
        return response()->json($item,200);
    }

    public function update($id, Request $request)
    {
        $this->validate($request,['product_id'=>'required','experience'=>'required','arrive_on_time'=>'required',
        'quality'=>'required','response_time'=>'required','comment'=>'required']);
        //$head = $request->all();
        $item = Review::findOrFail($id);
        $item->update($request->all());
        return $this->getById($item->id);
        // return response()->json($item, 200);
    }

    public function delete($id)
    {
        Review::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

?>