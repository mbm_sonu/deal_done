<?php
namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\Http\Request;
use App\SubCategory;

class SubCategoryController extends Controller
{
    public function getAll()
    {
        return response()->json(SubCategory::with('category')->get());
    }

    public function getById($id)
    {
        return response()->json(SubCategory::with('category')->findorfail($id),200);
    }

    // public function getByCategory($id)
    // {
    //     return response()->json(SubCategory::with('category')->findorfail($id),200);
    // }

    public function getProducts($id)
    {
        $products = SubCategory::findorfail($id)->products;
        $this->calculateDiscount($products);
        return response()->json($products,200);
    }
    public function create(Request $request)
    {
        $this->validate($request,['subcategory_name'=>'required','category_id'=>'required']);
        $item = SubCategory::create($request->all());
        return $this->getById($item->id);
    }

    public function update($id, Request $request)
    {
        $this->validate($request,['subcategory_name'=>'required','category_id'=>'required']);
        //$head = $request->all();
        $item = SubCategory::findOrFail($id);
        $item->update($request->all());
        return $this->getById($item->id);
        // return response()->json($item, 200);
    }

    public function delete($id)
    {
        SubCategory::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    private function calculateDiscount($products)
    {
        foreach($products as $key =>$product)
        {
            if($product->discount)
            {
                $start =  $product->discount->start_date;
                $end =  $product->discount->end_date;
                $start = strtotime($start); 
                $end = strtotime($end);
                $current = strtotime( date('Y-m-d'));

                if ($current >= $start && $current <= $end)
                {
                    $discount = $products[$key]['discount'];
                   // unset($products[$key]['discount']);
                    $products[$key]['discount_data'] = ['discounted_price'=>$product->price-($product->price*$discount->discount/100),
                        'end_date'=>$discount->end_date
                ];
                }
                else
                {
                    unset($products[$key]['discount']);
                }
            }
        }
    }
}

?>