<?php
namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as Controller;
use Illuminate\Http\Request;
use App\Custom_feature;

class Custom_featureController extends Controller
{
    public function getAll()
    {
        return response()->json(Custom_feature::with('product')->get());
    }

    public function getById($id)
    {
        return response()->json(Custom_feature::with('product')->findorfail($id),200);
    }
    
    public function getByProduct($id)
    {
        return response()->json(Custom_feature::with('product')->findorfail($id),200);
    }

    public function create(Request $request)
    {
        $this->validate($request,['key'=>'required','custom_feature_name'=>'required','product_id'=>'required']);
        $item = Custom_feature::create($request->all());
        return $this->getById($item->id);
    }

    public function update($id, Request $request)
    {
        $this->validate($request,['key'=>'required','custom_feature_name'=>'required','product_id'=>'required']);
                //$head = $request->all();
        $item = Custom_feature::findOrFail($id);
        $item->update($request->all());
        return $this->getById($item->id);
        // return response()->json($item, 200);
    }

    public function delete($id)
    {
        Custom_feature::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}

?>