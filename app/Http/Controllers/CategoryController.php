<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as Controller;
use App\Category;

use App\imgRoute as imgRoute;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class CategoryController extends Controller
{
    public function getAll()
    {
        return response()->json(Category::all());
    }

    public function getById($id)
    {
        return response()->json(Category::findorfail($id),200);
    }


    public function getProducts($id)
    {
        $products = Category::findorfail($id)->products;
        $this->calculateDiscount($products);
        return response()->json($products,200);
    }

    public function getBySubCategory($id)
    {
        return response()->json(Category::findorfail($id)->subCategory,200);
    }

    public function create(Request $request)
    {
        $this->validate($request,['category_name'=>'required','logo'=>'required']);
        $imagePath =  imgRoute::url().'placeholder.jpg';

        if($request->logo)
        {
            $name = imgRoute::saveImage($request->logo);
            if($name == ''){
                return response()->json(['logo' => 'File Uploaded is not an image!']);
            } else {

                $data = $request->toArray();
                $data['logo'] = $name;
                $item =Category::create($data);
                return $this->getById($item->id);
            }
        }
    }

    public function update($id, Request $request)
    {
        $this->validate($request,['category_name'=>'required','logo'=>'required']);
        $imagePath = imgRoute::url().'placeholder.jpg';

        if($request->logo)
        {
            $data = explode('/', explode(';' , $request->logo)[0]);
            $extension = $data[1];
            $type = explode(':', $data[0])[1];
            if($type != 'image'){
                
                return response()->json(['logo' => 'File Uploaded is not an image!']);
            } else {

                $a=explode("/",$request->image_path)[5];
                $b=explode("/",$request->image_path)[6];

                $image_path_del=$a."/".$b;

                if(File::exists($image_path_del)){
                    File::delete($image_path_del);
                }

                Image::configure(array('driver' => 'gd'));
                $random_number = mt_rand( 10000000, 99999999);
                $name = $random_number . '_' .time() . '.' . $extension;
                $image_Path = imgRoute::directory() . $name;
                $imagePath = $name;
                $image = Image::make($request->logo)->save($image_Path);
                $request->logo = $image_Path;
                
                $data = $request->toArray();
                $data['logo'] = $name;
                
                $item = Category::findOrFail($id);
                $item->update($data);
                return $this->getById($item->id);
            }
    }
        
    }

    public function delete($id)
    { 
        $item = Category::findOrFail($id);
        $a=explode("/",$item->logo)[5];
        $b=explode("/",$item->logo)[6];

        $image_path_del=$a."/".$b;

        if(File::exists($image_path_del)){
            File::delete($image_path_del);
        }
        Category::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    private function calculateDiscount($products)
    {
        foreach($products as $key =>$product)
        {
            if($product->discount)
            {
                $start =  $product->discount->start_date;
                $end =  $product->discount->end_date;
                $start = strtotime($start); 
                $end = strtotime($end);
                $current = strtotime( date('Y-m-d'));

                if ($current >= $start && $current <= $end)
                {
                    $discount = $products[$key]['discount'];
                   // unset($products[$key]['discount']);
                    $products[$key]['discount_data'] = ['discounted_price'=>$product->price-($product->price*$discount->discount/100),
                        'end_date'=>$discount->end_date
                ];
                }
                else
                {
                    unset($products[$key]['discount']);
                }
            }
        }
    }
}

?>