<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    public $table = "table_subcategory";
    protected $fillable = ['id','subcategory_name','category_id'];
    protected $hidden = ['created_at','updated_at'];

    public function category(){
        return $this->belongsTo(Category::class,'category_id')->select(['id','category_name','logo']);
    }

    public function products()
    {
        return $this->hasMany(Product::class,'subcategory_id');
    }
}

?>