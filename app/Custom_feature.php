<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Custom_feature extends Model
{
    public $table = "table_custom_feature";
    protected $fillable = ['id','key','custom_feature_name','product_id'];
    protected $hidden = ['created_at','updated_at'];

    public function product(){
        return $this->belongsTo(Product::class,'product_id')->select(['id','company_id','title','subtitle','condition','feature_photo',
        'price','key_words','model','brand','description']);
    }
}

?>