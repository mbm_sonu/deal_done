<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class State extends Model{

    public $table="table_state";
    protected $fillable=['id','state_name','country_id'];
    protected $hidden=['created_at','updated_at'];

    public function  country(){
        return $this->belongsTo(Country::class,'country_id')->select(['id','country_name']);
    }

    public function city()
    {
        return $this->hasMany(City::class,'state_id');
    }
}



?>