<?php
namespace App;
use Intervention\Image\ImageManagerStatic as Image;

class imgRoute{

    public static function  url(){
    return "http://192.168.10.10/deal_done/public/".self::directory();
}
public static function  directory(){
    return "images/";
}

public static function saveImage($logo)
{
    $data = explode('/', explode(';' , $logo)[0]);
    $extension = $data[1];
    $type = explode(':', $data[0])[1];
    if($type != 'image'){
        return "";
    } else {

        Image::configure(array('driver' => 'gd'));
        $random_number = mt_rand( 10000000, 99999999);
        $name = $random_number . '_' .time() . '.' . $extension;
        $image_path = imgRoute::directory().$name;
        $image = Image::make($logo)->save($image_path);

        return $name;
    }
}
}
?>