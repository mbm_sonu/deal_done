<?php
namespace App;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    public $table = "table_product";

    protected $fillable= ['id','user_id','title','subtitle','condition','condition_name','feature_photo','category_id','subcategory_id',
    'price','quantity','keywords','description',
    //'other_photos',
    'specifications'];

    protected $hidden = ['created_at','updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id')->select(['id','name','number']);
    }
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id')->select(['id','category_name','logo']);
    }
    
    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class,'subcategory_id')->select(['id','subcategory_name','category_id']);
    }
    
    public function photos()
    {
        return $this->hasMany(Photos_table::class,'product_id');
    }
    public function discount()
    {
        return $this->hasOne(Discount::class,'product_id');
    }
    public function custom_feature()
    {
        return $this->hasMany(Custom_feature::class,'product_id');
    }

    public function getFeaturePhotoAttribute($value)
    {
        return imgRoute::url().$value;
    }
    
}

?>