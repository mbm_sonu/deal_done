<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    public $table = "table_discount";
    protected $fillable = ['id','product_id','discount','start_date','end_date','is_fixed'];
    protected $hidden = ['created_at','updated_at'];

    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }


}

?>