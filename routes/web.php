<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix'=>'vendor'],function () use ($router){

    $router->get('/',['uses'=>'VendorController@getAll']);
    $router->get('/{id}',['uses'=>'VendorController@getById']);
    $router->get('/{id}/company',['uses'=>'VendorController@getCompanies']);
    $router->post('/',['uses'=>'VendorController@create']);
    $router->put('/{id}',['uses'=>'VendorController@update']);
    $router->delete('/{id}',['uses'=>'VendorController@delete']);
    $router->get('/{id}/products',['uses'=>'VendorController@getProducts']);


});

$router->group(['prefix'=>'country'],function () use($router){

    $router->get('/',['uses'=>'CountryController@getAll']);
    $router->get('/{id}',['uses'=>'CountryController@getById']);
    $router->get('/{id}/state',['uses'=>'CountryController@getStates']);
    //$router->post('/',['uses'=>'CountryController@create','middleware'=>'auth']);
    $router->post('/',['uses'=>'CountryController@create']);    
    $router->put('/{id}',['uses'=>'CountryController@update']);
    $router->delete('/{id}',['uses'=>'CountryController@delete']);

});

$router->group(['prefix'=>'state'],function () use($router){

    $router->get('/',['uses'=>'StateController@getAll']);
    $router->get('/{id}',['uses'=>'StateController@getById']);
    $router->get('/{id}/city',['uses'=>'StateController@getCities']);
    $router->get('/{id}/country',['uses'=>'StateController@getCountries']);
    $router->post('/',['uses'=>'StateController@create']);
    $router->put('/{id}',['uses'=>'StateController@update']);
    $router->delete('/{id}',['uses'=>'StateController@delete']);

});


$router->group(['prefix'=>'city'],function () use($router){

    $router->get('/',['uses'=>'CityController@getAll']);
    $router->get('/{id}',['uses'=>'CityController@getById']);
    $router->get('/{id}/address',['uses'=>'CityController@getAddresses']);
    $router->get('/{id}/state',['uses'=>'CityController@getStates']);
    // $router->get('/{id}/product',['uses'=>'CityController@getProducts']);
    $router->post('/',['uses'=>'CityController@create']);
    $router->put('/{id}',['uses'=>'CityController@update']);
    $router->delete('/{id}',['uses'=>'CityController@delete']);

});

$router->group(['prefix'=>'address'],function () use($router){

    $router->get('/',['uses'=>'AddressController@getAll']);
    $router->get('/{id}',['uses'=>'AddressController@getById']);
    $router->get('/{id}/company',['uses'=>'AddressController@getCompanies']);
    $router->get('/{id}/city',['uses'=>'AddressController@getCities']);
    $router->post('/',['uses'=>'AddressController@create']);
    $router->put('/{id}',['uses'=>'AddressController@update']);
    $router->delete('/{id}',['uses'=>'AddressController@delete']);
    $router->get('get_by_user/{id}',['uses'=>'AddressController@getByUser']);

});


$router->group(['prefix'=>'company'],function () use($router){

    //$router->get('/',['uses'=>'Company_profile_Controller@getAll']);
    $router->get('/{id}',['uses'=>'Company_profile_Controller@getById']);
    //$router->get('/{id}/address',['uses'=>'Company_profile_Controller@getAddresses']);
    $router->get('/get_by_user/{id}',['uses'=>'Company_profile_Controller@getByUser']);
    //$router->get('/{id}/product',['uses'=>'Company_profile_Controller@getProducts']);
    
    $router->post('/',['uses'=>'Company_profile_Controller@create']);
    $router->put('/{id}',['uses'=>'Company_profile_Controller@update']);
    $router->delete('/{id}',['uses'=>'Company_profile_Controller@delete']);

});

$router->group(['prefix'=>'product'],function () use ($router){

    $router->get('/',['uses'=>'ProductController@getAll']);
    $router->get('/get',['uses'=>'ProductController@getWith']);
    $router->get('/{id}',['uses'=>'ProductController@getById']);
    $router->get('/{id}/photos',['uses'=>'ProductController@getByPhotos']);
    $router->get('/{id}/discount',['uses'=>'ProductController@getByDiscount']);
    $router->get('/{id}/category',['uses'=>'ProductController@getByCategory']);
    $router->get('/{id}/subCategory',['uses'=>'ProductController@getBySubCategory']);
    $router->get('/{id}/custom_feature',['uses'=>'ProductController@getByCustom']);
    $router->get('get_by_user/{id}',['uses'=>'ProductController@getByUser']);
    

    $router->get('/q/{data}',['uses'=>'ProductController@getProductLike']);
    $router->post('/',['uses'=>'ProductController@create']);
    $router->put('/{id}',['uses'=>'ProductController@update']);
    $router->delete('/{id}',['uses'=>'ProductController@delete']);

});

$router->group(['prefix'=>'discount'],function () use ($router){

    $router->get('/',['uses'=>'DiscountController@getAll']);
    $router->get('/{id}',['uses'=>'DiscountController@getById']);
    $router->get('/{id}/product',['uses'=>'DiscountController@getByProduct']);
    $router->post('/',['uses'=>'DiscountController@create']);
    $router->put('/{id}',['uses'=>'DiscountController@update']);
    $router->delete('/{id}',['uses'=>'DiscountController@delete']);

});


$router->group(['prefix'=>'photos'],function () use ($router){

    $router->get('/',['uses'=>'PhotosController@getAll']);
    $router->get('/{id}',['uses'=>'PhotosController@getById']);
    $router->get('/{id}/product',['uses'=>'PhotosController@getByProduct']);
    $router->post('/',['uses'=>'PhotosController@create']);
    $router->put('/{id}',['uses'=>'PhotosController@update']);
    $router->delete('/{id}',['uses'=>'PhotosController@delete']);

});



$router->group(['prefix'=>'custom_feature'],function () use ($router){

    $router->get('/',['uses'=>'Custom_featureController@getAll']);
    $router->get('/{id}',['uses'=>'Custom_featureController@getById']);
    $router->get('/{id}/product',['uses'=>'Custom_featureController@getByProduct']);
    $router->post('/',['uses'=>'Custom_featureController@create']);
    $router->put('/{id}',['uses'=>'Custom_featureController@update']);
    $router->delete('/{id}',['uses'=>'Custom_featureController@delete']);

});

$router->group(['prefix'=>'category'],function () use ($router){

    $router->get('/',['uses'=>'CategoryController@getAll']);
    $router->get('/{id}',['uses'=>'CategoryController@getById']);
    $router->get('/{id}/subCategory',['uses'=>'CategoryController@getBySubCategory']);
    $router->get('/{id}/products',['uses'=>'CategoryController@getProducts']);
    $router->post('/',['uses'=>'CategoryController@create']);
    $router->put('/{id}',['uses'=>'CategoryController@update']);
    $router->delete('/{id}',['uses'=>'CategoryController@delete']);

});


$router->group(['prefix'=>'subCategory'],function () use ($router){

    $router->get('/',['uses'=>'SubCategoryController@getAll']);
    $router->get('/{id}',['uses'=>'SubCategoryController@getById']);
    $router->get('/{id}/category',['uses'=>'SubCategoryController@getByCategory']);
    $router->get('/{id}/products',['uses'=>'SubCategoryController@getProducts']);
    $router->post('/',['uses'=>'SubCategoryController@create']);
    $router->put('/{id}',['uses'=>'SubCategoryController@update']);
    $router->delete('/{id}',['uses'=>'SubCategoryController@delete']);

});

$router->group(['prefix'=>'user'],function () use ($router){

    $router->post('/',['uses'=>'UserController@create']);
    $router->get('/',['uses'=>'UserController@getAll']);
    $router->post('/authenticate',['uses'=>'UserController@authenticate']);
    $router->get('/',['uses'=>'UserController@getAll']);
    $router->get('/{id}/products',['uses'=>'UserController@getProducts']);
    $router->get('/{id}/address',['uses'=>'UserController@getAddress']);
    $router->get('/{id}/company',['uses'=>'UserController@getCompany']);
    $router->get('/vendors',['uses'=>'UserController@getAllVendors']);
    $router->get('/get/{token}',['uses'=>'UserController@getByToken']);
    

    
   // $router->put('/{id}',['uses'=>'SubCategoryController@update']);
   // $router->delete('/{id}',['uses'=>'SubCategoryController@delete']);

});

$router->group(['prefix'=>'review'],function () use ($router){

    $router->get('/',['uses'=>'ReviewController@getAll']);
    $router->get('/{id}',['uses'=>'ReviewController@getById']);
    $router->get('/{id}/product',['uses'=>'ReviewController@getByProduct']);
    $router->post('/',['uses'=>'ReviewController@create']);
    $router->put('/{id}',['uses'=>'ReviewController@update']);
    $router->delete('/{id}',['uses'=>'ReviewController@delete']);

});
