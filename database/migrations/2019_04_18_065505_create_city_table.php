<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_city', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('city_name');
            $table->bigInteger('state_id')->unsigned()->index();
            $table->foreign('state_id')->references('id')->on('table_state');
            $table->timestamps();
        });

        DB::table('table_city')->insert([
            ['state_id' => '1','city_name' => 'Lahore',
            'created_at'=>Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon\Carbon::now()->toDateTimeString(),
            ],
            ['state_id' => '1','city_name' => 'Gujranwala',
            'created_at'=>Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon\Carbon::now()->toDateTimeString(),
            ],
            ['state_id' => '1','city_name' => 'Faisalabad',
            'created_at'=>Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon\Carbon::now()->toDateTimeString(),
            ],
            ['state_id' => '3','city_name' => 'Karachi',
            'created_at'=>Carbon\Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon\Carbon::now()->toDateTimeString(),
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_city');
    }
}
