<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomFeature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_custom_feature', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key');
            $table->string('custom_feature_name');
            $table->bigInteger('product_id')->unsigned()->index();
           
            $table->foreign('product_id')->references('id')->on('table_product');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_custom_feature');
    }
}
