<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_photos_table', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('link');
            $table->bigInteger('product_id')->unsigned()->index();
           
            $table->foreign('product_id')->references('id')->on('table_product');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_photos_table');
    }
}
