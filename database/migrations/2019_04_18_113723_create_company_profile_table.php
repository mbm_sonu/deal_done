<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_company_profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('table_user');

            $table->bigInteger('city_id')->unsigned()->index();
            $table->foreign('city_id')->references('id')->on('table_city');
            $table->string('street_name')->nullable();

            $table->string('logo')->nullable();
            $table->string('tag_line')->nullable();
            $table->string('description')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('company_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_company_profile');
    }
}
